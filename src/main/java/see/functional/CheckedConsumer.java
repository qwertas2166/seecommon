package see.functional;

@FunctionalInterface
public interface CheckedConsumer<Input> {
    void accept(Input input) throws Exception;

}
