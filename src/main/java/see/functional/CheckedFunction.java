package see.functional;

@FunctionalInterface
public interface CheckedFunction<Input, Return> {
    Return apply(Input input) throws Exception;
}
