package see.functional;

@FunctionalInterface
public interface CheckedSupplier<Return> {
    Return get() throws Exception;
}
