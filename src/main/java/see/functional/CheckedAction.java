package see.functional;

@FunctionalInterface
public interface CheckedAction {
    void act() throws Exception;
}
