package see.collection.search;

import java.util.TreeMap;
import java.util.function.Supplier;

public class BTreeMap<TKeyElement extends Comparable<TKeyElement>, TValue> extends TreeMap<TKeyElement[], TValue> {

    public BTreeMap() {
        super((array1, array2) -> {
            if (array1.length != array2.length) {
                throw new RuntimeException("Not same length for comparable arrays");
            }
            for (int i = 0; i < array1.length; i++) {
                int compareResult = array1[i].compareTo(array2[i]);
                if (compareResult != 0) {
                    return compareResult;
                }
            }
            return 0;
        });
    }

    public TValue get(Supplier<TKeyElement>... getAttributes) {
        TKeyElement[] key = getKey(getAttributes);
        return this.get(key);
    }

    public TValue put(Supplier<TKeyElement>[] getAttributes, TValue value) {
        TKeyElement[] key = getKey(getAttributes);
        return this.put(key, value);
    }

    public TValue putIf(Supplier<TValue> ifAbsent, Supplier<TKeyElement>... getAttributes) {
        TKeyElement[] key = getKey(getAttributes);
        TValue value = this.get(key);
        if (value == null) {
            TValue newValue = ifAbsent.get();
            this.put(key, newValue);
            return newValue;
        } else {
            return value;
        }
    }

    private TKeyElement[] getKey(Supplier<TKeyElement>[] getAttributes) {
        @SuppressWarnings("unchecked")
        TKeyElement[] key = (TKeyElement[]) new Comparable[getAttributes.length];
        for (int i = 0; i < getAttributes.length; i++) {
            key[i] = getAttributes[i].get();
        }
        return key;
    }
}
