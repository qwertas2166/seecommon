package see.collection.search;

import java.util.List;

public class Stack<T> implements SerialCollection<T> {
    private final java.util.Stack<T> stack = new java.util.Stack<>();

    @Override
    public T pop() {
        return this.stack.pop();
    }

    @Override
    public void push(T item) {
        this.stack.push(item);
    }

    @Override
    public void pushAll(List<T> items) {
        this.stack.addAll(items);
    }

    @Override
    public boolean isEmpty() {
        return this.stack.isEmpty();
    }
}
