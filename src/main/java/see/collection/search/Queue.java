package see.collection.search;

import java.util.List;

public class Queue<T> implements SerialCollection<T> {
    private final java.util.Queue<T> queue = new java.util.LinkedList<>();

    @Override
    public T pop() {
        return this.queue.poll();
    }

    @Override
    public void push(T item) {
        this.queue.offer(item);
    }

    @Override
    public void pushAll(List<T> items) {
        this.queue.addAll(items);
    }

    @Override
    public boolean isEmpty() {
        return this.queue.isEmpty();
    }
}
