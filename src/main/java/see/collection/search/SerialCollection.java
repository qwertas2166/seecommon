package see.collection.search;

import java.util.List;

public interface SerialCollection<T>{
    T pop();

    void push(T item);

    void pushAll(List<T> items);

    boolean isEmpty();
}
