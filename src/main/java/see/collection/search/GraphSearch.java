package see.collection.search;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GraphSearch {

//    public static <T, R> void bfs(T root,
//                                  Function<T, List<T>> getNodes,
//                                  BiFunction<R, T, R> onFound) {
//        Queue<T> queue = new Queue<>();
//        queue.push(root);
//        graphSearch(queue, getNodes, onFound);
//    }


    public static <Parent, Current> void dfs(Current root,
                                             Function<Current, List<Current>> getNodes,
                                             BiConsumer<GraphSearchContext<Parent>, Current> onFound) {
        Stack<Pair<GraphSearchContext<Parent>, Current>> stack = new Stack<>();
        GraphSearchContext<Parent> graphSearchContext = new GraphSearchContext<>();
        stack.push(new Pair<>(graphSearchContext, root));
        graphSearch(stack, getNodes, onFound);
    }

    public static <Parent, Current> void graphSearch(SerialCollection<Pair<GraphSearchContext<Parent>, Current>> collection,
                                                     Function<Current, List<Current>> getNodes,
                                                     BiConsumer<GraphSearchContext<Parent>, Current> onFound) {
        while (!collection.isEmpty()) {
            Pair<GraphSearchContext<Parent>, Current> item = collection.pop();
            onFound.accept(item.left(), item.right());
            if (item.left().isInterrupt()) {
                break;
            }
            List<Current> items = getNodes.apply(item.right());
            collection.pushAll(items.stream().map(i -> new Pair<>(item.left(), i)).collect(Collectors.toList()));
        }
    }

    public static class GraphSearchContext<Parent> {
        private boolean interrupt;
        private Parent previous;

        public Parent getPrevious() {
            return previous;
        }

        public void setPrevious(Parent previous) {
            this.previous = previous;
        }

        public boolean isInterrupt() {
            return interrupt;
        }

        public void setInterrupt(boolean interrupt) {
            this.interrupt = interrupt;
        }
    }
}
