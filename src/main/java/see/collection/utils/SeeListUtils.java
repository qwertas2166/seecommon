package see.collection.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.*;

public class SeeListUtils {
    public static <T> T getFirst(T obj, List<T> list) {
        for (T element : list) {
            if (element.equals(obj)) {
                return element;
            }
        }
        return null;
    }

    public static <T> T getFirst(List<T> list, Predicate<T> compare) {
        for (T element : list) {
            if (compare.test(element)) {
                return element;
            }
        }
        return null;
    }

    public static <T> boolean contains(List<T> list, Predicate<T> compare) {
        return getFirst(list, compare) != null;
    }

    public static <T, R> R getFirstWithReturn(List<T> list, Function<T, R> compareWithReturn) {
        for (T element : list) {
            R result;
            if ((result = compareWithReturn.apply(element)) != null) {
                return result;
            }
        }
        return null;
    }

    public static <T> T getFirstWithDeletion(List<T> list, Predicate<T> compare) {
        Iterator<T> iterator = list.iterator();
        while (iterator.hasNext()) {
            T element = iterator.next();
            if (compare.test(element)) {
                iterator.remove();
                return element;
            }
        }
        return null;
    }

    public static <T> List<T> getAll(List<T> list, Predicate<T> compare) {
        List<T> result = new ArrayList<>();
        for (T element : list) {
            if (compare.test(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public static <T> List<T> getAllWithDeletion(List<T> list, Predicate<T> compare) {
        List<T> result = new ArrayList<>();
        Iterator<T> iterator = list.iterator();
        while (iterator.hasNext()) {
            T element = iterator.next();
            if (compare.test(element)) {
                result.add(element);
                iterator.remove();
            }
        }
        return result;
    }

    public static <F, S> void searchWithDeletion(List<F> firstList, LinkedList<S> secondList, BiPredicate<F, S> compare, BiConsumer<F, S> action) {
        for (F first : firstList) {
            Iterator<S> iterator = secondList.iterator();
            while (iterator.hasNext()) {
                S second = iterator.next();
                if (compare.test(first, second)) {
                    action.accept(first, second);
                    iterator.remove();
                }
            }
        }
    }



    public static <T, R> List<R> getAllWithReturn(List<T> list, Function<T, R> compareWithReturn) {
        List<R> result = new ArrayList<>();
        for (T element : list) {
            R singleReturn;
            if ((singleReturn = compareWithReturn.apply(element)) != null) {
                result.add(singleReturn);
            }
        }
        return result;
    }

    public static <T> void getAllWithPostAction(List<T> list, Predicate<T> compare, Consumer<T> postActionIfTrue) {
        for (T element : list) {
            if (compare.test(element)) {
                postActionIfTrue.accept(element);
            }
        }
    }

    public static <T> List<T> getNotEquals(final List<T> first, final List<T> second) {
        List<T> result = new LinkedList<>(second);
        for (T element : first) {
            getFirstWithDeletion(result, element::equals);
        }
        return result;
    }
}
