package see.collection.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * array utils
 */
public class SeeArrayUtils {
    public static <T> T getFirst(T obj, T[] array) {
        for (T element : array) {
            if (element.equals(obj)) {
                return element;
            }
        }
        return null;
    }

    public static <T> boolean contains(T obj, T[] list) {
        return getFirst(obj, list) != null;
    }

    public static <T> T getFirst(T[] array, Predicate<T> compare) {
        for (T element : array) {
            if (compare.test(element)) {
                return element;
            }
        }
        return null;
    }

    public static <T> List<T> getAll(T[] array, Predicate<T> compare) {
        List<T> result = new ArrayList<>();
        for (T element : array) {
            if (compare.test(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public static <T> T[] concatenate(T[] first, T[] second) {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }
}
