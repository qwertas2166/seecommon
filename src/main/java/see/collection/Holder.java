package see.collection;

public class Holder<T> {
    private T instance;

    public Holder() {
    }

    public Holder(T instance) {
        this.instance = instance;
    }

    public void set(T instance) {
        if (this.instance == null) {
            this.instance = instance;
        }
    }

    public T get() {
        return this.instance;
    }
}
